package ru.mtumanov.tm.api.model;

public interface IHaveName {

    String getName();

    void setName(String name);

}
