package ru.mtumanov.tm.api.service;

import java.util.Comparator;
import java.util.List;

import ru.mtumanov.tm.enumerated.Sort;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.AbstractUserOwnedModel;

public interface IUserOwnerService<M extends AbstractUserOwnedModel> extends IService<M> {

    List<M> findAll(String userId) throws AbstractException;

    M add(String userId, M model) throws AbstractException;

    List<M> findAll(String userId, Comparator<M> comparator) throws AbstractException;

    List<M> findAll(String userId, Sort sort) throws AbstractException;

    M findOneById(String userId, String id) throws AbstractException;

    M findOneByIndex(String userId, Integer index) throws AbstractException;

    M remove(String userId, M model) throws AbstractException;

    M removeById(String userId, String id) throws AbstractException;

    M removeByIndex(String userId, Integer index) throws AbstractException;

    void clear(String userId) throws AbstractException;

    int getSize(String userId) throws AbstractException;

    boolean existById(String userId, String id) throws AbstractException;
    
}
