package ru.mtumanov.tm.command.project;

import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class ProjectStartByIndexCommnd extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Start project by index";
    }

    @Override
    public String getName() {
        return "project-start-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        getProjectService().changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);
    }
    
}
