package ru.mtumanov.tm.command.project;

import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.api.service.IProjectTaskService;
import ru.mtumanov.tm.command.AbstractCommand;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus());
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }
    
}
