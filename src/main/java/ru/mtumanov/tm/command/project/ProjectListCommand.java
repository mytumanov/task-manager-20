package ru.mtumanov.tm.command.project;

import java.util.Arrays;
import java.util.List;

import ru.mtumanov.tm.enumerated.Sort;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.util.TerminalUtil;

public class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Show list projects";
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        int index = 1;
        final List<Project> projects = getProjectService().findAll(userId, sort);
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }
    
}
