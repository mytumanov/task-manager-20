package ru.mtumanov.tm.command.task;

import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskUpdateById extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Update task by id";
    }

    @Override
    public String getName() {
        return "task-update-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().updateById(userId, id, name, description);
    }
    
}
