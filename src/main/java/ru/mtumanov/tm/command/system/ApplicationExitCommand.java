package ru.mtumanov.tm.command.system;

public class ApplicationExitCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Close application";
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public void execute() {
        System.exit(0);
    }
    
}
