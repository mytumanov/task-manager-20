package ru.mtumanov.tm.exception.user;

public class RoleEmptyException extends AbstractUserException {
    
    public RoleEmptyException() {
        super("ERROR! Role is empty!");
    }
    
}
