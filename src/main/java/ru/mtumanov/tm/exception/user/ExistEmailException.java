package ru.mtumanov.tm.exception.user;

public class ExistEmailException extends AbstractUserException {

    public ExistEmailException() {
        super("ERROR! Email already exists!");
    }
    
}
