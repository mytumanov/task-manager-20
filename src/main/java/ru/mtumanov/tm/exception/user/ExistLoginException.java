package ru.mtumanov.tm.exception.user;

public class ExistLoginException extends AbstractUserException {

    public ExistLoginException() {
        super("ERROR! Login already exists!");
    }
    
}
