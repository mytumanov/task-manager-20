package ru.mtumanov.tm.repository;

import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {

}
