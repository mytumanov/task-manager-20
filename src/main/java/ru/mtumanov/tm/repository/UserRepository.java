package ru.mtumanov.tm.repository;

import ru.mtumanov.tm.api.repository.IUserRepository;
import ru.mtumanov.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        for (User user : models) {
            if (user.getLogin().equals(login))
                return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (User user : models) {
            if (user.getEmail().equals(email))
                return user;
        }
        return null;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        for (User user : models) {
            if (user.getLogin().equals(login))
                return true;
        }
        return false;
    }

    @Override
    public Boolean isEmailExist(final String email) {
        for (User user : models) {
            if (user.getEmail().equals(email))
                return true;
        }
        return false;
    }

}
