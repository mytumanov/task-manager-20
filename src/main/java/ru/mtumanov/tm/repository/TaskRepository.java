package ru.mtumanov.tm.repository;

import java.util.ArrayList;
import java.util.List;

import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.model.Task;

public final class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final List<Task> taskList = new ArrayList<>();
        for (final Task task : models) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId) && task.getUserId().equals(userId)) taskList.add(task);
        }
        return taskList;
    }

}
